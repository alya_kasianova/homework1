package myproject.automation.homework1.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Nav_Sidebar {
    WebDriver driver;
    WebDriverWait wait;

    private By dashboardMenuItem = By.cssSelector("#tab-AdminDashboard  a");
    private By ordersMenuItem = By.xpath("//li[@id = 'subtab-AdminParentOrders']/a");
    private By catalogMenuItem = By.xpath("//span[contains(text(), 'Каталог')]//ancestor::a");
    private By clientsMenuItem = By.xpath("//span[contains(text(), 'Клиенты')]//ancestor::a");
    private By supportServiceMenuItem = By.xpath("//li[@id = 'subtab-AdminParentCustomerThreads']/a");
    private By statisticsMenuItem = By.xpath("//li[@id = 'subtab-AdminStats']/a");
    private By modulesMenuItem = By.xpath("//span[contains(text(), 'Modules')]//ancestor::a");
    private By designMenuItem = By.xpath("//span[contains(text(), 'Design')]//ancestor::a");
    private By deliveryMenuItem = By.xpath("//nav[@id='nav-sidebar']//span[contains(text(), 'Доставка')]//ancestor::a");
    private By paymentMethodsMenuItem = By.xpath("//li[@id = 'subtab-AdminParentPayment']/a");
    private By internationalMenuItem = By.xpath("//li[@id = 'subtab-AdminInternational']/a");
    private By shopParametersMenuItem = By.xpath("//li[@id = 'subtab-ShopParameters']/a");
    private By configurationMenuItem = By.id("subtab-AdminAdvancedParameters");

    public Nav_Sidebar(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
    }

    public void clickOnDashboardMenuItem(){
        wait.until(ExpectedConditions.elementToBeClickable(dashboardMenuItem));
        driver.findElement(dashboardMenuItem).click();
    }

    public void clickOnOrdersMenuItem(){
        wait.until(ExpectedConditions.elementToBeClickable(ordersMenuItem));
        driver.findElement(ordersMenuItem).click();
    }

    public void clickOnCatalogMenuItem(){
        wait.until(ExpectedConditions.elementToBeClickable(catalogMenuItem));
        driver.findElement(catalogMenuItem).click();
    }

    public void clickOnClientsMenuItem(){
        wait.until(ExpectedConditions.elementToBeClickable(clientsMenuItem));
        driver.findElement(clientsMenuItem).click();
    }

    public void clickOnSupportServiceMenuItem(){
        wait.until(ExpectedConditions.elementToBeClickable(supportServiceMenuItem));
        driver.findElement(supportServiceMenuItem).click();
    }

    public void clickOnStatisticsMenuItem(){
        wait.until(ExpectedConditions.elementToBeClickable(statisticsMenuItem));
        driver.findElement(statisticsMenuItem).click();
    }

    public void clickOnModulesMenuItem(){
        wait.until(ExpectedConditions.elementToBeClickable(modulesMenuItem));
        driver.findElement(modulesMenuItem).click();
    }

    public void clickOnDesignMenuItem() {
        wait.until(ExpectedConditions.elementToBeClickable(designMenuItem));
        driver.findElement(designMenuItem).click();
    }

    public void clickOnDeliveryMenuItem(){
        wait.until(ExpectedConditions.elementToBeClickable(deliveryMenuItem));
        driver.findElement(deliveryMenuItem).click();
    }

    public void clickOnPaymentMethodsMenuItem(){
        wait.until(ExpectedConditions.elementToBeClickable(paymentMethodsMenuItem));
        driver.findElement(paymentMethodsMenuItem).click();
    }

    public void clickOnInternationalMenuItem(){
        wait.until(ExpectedConditions.elementToBeClickable(internationalMenuItem));
        driver.findElement(internationalMenuItem).click();
    }

    public void clickOnShopParametersMenuItem(){
        wait.until(ExpectedConditions.elementToBeClickable(shopParametersMenuItem));
        driver.findElement(shopParametersMenuItem).click();
    }

    public void clickOnConfigurationMenuItem(){
        wait.until(ExpectedConditions.elementToBeClickable(configurationMenuItem));
        driver.findElement(configurationMenuItem).click();
    }
}