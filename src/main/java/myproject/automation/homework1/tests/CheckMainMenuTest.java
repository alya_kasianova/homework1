package myproject.automation.homework1.tests;

import myproject.automation.homework1.BaseScript;
import myproject.automation.homework1.pages.LoginPage;
import myproject.automation.homework1.pages.Nav_Sidebar;
import myproject.automation.homework1.pages.PageContent;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CheckMainMenuTest extends BaseScript{
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = getDriver();
        WebDriverWait wait = createWaitDriver(driver);
        logIntoSystem(driver);

        Nav_Sidebar sidebar = new Nav_Sidebar(driver, wait);
        sidebar.clickOnDashboardMenuItem();

        PageContent content = new PageContent(driver, wait);
        content.displayTitleInConsole(driver);
        ValidateURl(driver);

        sidebar.clickOnOrdersMenuItem();
        content.displayTitleInConsole(driver);
        ValidateURl(driver);

        sidebar.clickOnCatalogMenuItem();
        content.displayTitleInConsole(driver);
        ValidateURl(driver);

        sidebar.clickOnClientsMenuItem();
        content.displayTitleInConsole(driver);
        ValidateURl(driver);

        sidebar.clickOnSupportServiceMenuItem();
        content.displayTitleInConsole(driver);
        ValidateURl(driver);

        sidebar.clickOnStatisticsMenuItem();
        content.displayTitleInConsole(driver);
        ValidateURl(driver);

        sidebar.clickOnModulesMenuItem();
        content.displayTitleInConsole(driver);
        ValidateURl(driver);

        sidebar.clickOnDesignMenuItem();
        content.displayTitleInConsole(driver);
        ValidateURl(driver);

        sidebar.clickOnDeliveryMenuItem();
        content.displayTitleInConsole(driver);
        ValidateURl(driver);

        sidebar.clickOnPaymentMethodsMenuItem();
        content.displayTitleInConsole(driver);
        ValidateURl(driver);

        sidebar.clickOnInternationalMenuItem();
        content.displayTitleInConsole(driver);
        ValidateURl(driver);

        sidebar.clickOnShopParametersMenuItem();
        content.displayTitleInConsole(driver);
        ValidateURl(driver);

        sidebar.clickOnConfigurationMenuItem();
        content.displayTitleInConsole(driver);
        ValidateURl(driver);

        quitDriver(driver);
    }

    private static void logIntoSystem(WebDriver driver){
        String email = "webinar.test@gmail.com";
        String password = "Xcg7299bnSmMuRLp9ITw";

        LoginPage loginPage = new LoginPage(driver);
        loginPage.goToLoginPage();
        loginPage.fillEmailInput(email);
        loginPage.fillPasswordInput(password);
        loginPage.clickLoginBtn();
    }

    private static void ValidateURl(WebDriver driver){
        String url = driver.getCurrentUrl();
        refreshPage(driver);
        Assert.assertEquals(url, driver.getCurrentUrl());
    }
}
