package myproject.automation.homework1;

import myproject.automation.homework1.utils.Properties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;

/**
 * Base script functionality, can be used for all Selenium scripts.
 */
public abstract class BaseScript {
    public static final long DEFAULT_SLEEP_TIMEOUT = 30;
    /**
     *
     * @return New instance of {@link WebDriver} object.
     */
    public static WebDriver getDriver() {
        String browser = Properties.getBrowser();
        switch(browser){
            case "chrome":
                System.setProperty("webdriver.chrome.driver",
                                   new File(BaseScript.class.getResource("/chromedriver.exe").getFile()).getPath());
                return new ChromeDriver();
            case "firefox":
                System.setProperty("webdriver.gecko.driver",
                                    new File(BaseScript.class.getResource("/geckodriver.exe").getFile()).getPath());
                return new FirefoxDriver();
        }

        throw new UnsupportedOperationException("Method doesn't return WebDriver instance");
    }

    /**
     * Refreshes psge
     * @param driver - the driver
     */
    public static void refreshPage(WebDriver driver){ driver.navigate().refresh();}

    /**
     * Exists the Browser
     * @param driver - the driver
     */
    public static void quitDriver(WebDriver driver){
        driver.quit();
    }

    /**
     * Creates wait driver
     * @param driver - the driver
     * @return wait driver
     */
    public static WebDriverWait createWaitDriver(WebDriver driver){
        return new WebDriverWait(driver, DEFAULT_SLEEP_TIMEOUT);
    }
}