package myproject.automation.homework1.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Nav_Header {
    WebDriver driver;
    WebDriverWait wait;
    private By logo = By.cssSelector("#employee_infos a");
    private By exitBtn = By.id("header_logout");

    public Nav_Header(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
    }

    public void clickOnLogo(){
        wait.until(ExpectedConditions.elementToBeClickable(logo));
        driver.findElement(logo).click();
    }

    public void clickOnExitBtn(){
        driver.findElement(exitBtn).click();
    }
}