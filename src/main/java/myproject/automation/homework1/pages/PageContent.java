package myproject.automation.homework1.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageContent {
    WebDriver driver;
    WebDriverWait wait;

    private By title = By.xpath("//h2[contains(@class,'title')]");

    public PageContent(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
    }

    public void displayTitleInConsole(WebDriver driver){
        wait.until(ExpectedConditions.visibilityOfElementLocated(title));
        System.out.println("Title: " + driver.findElement(title).getText().trim());
    }
}