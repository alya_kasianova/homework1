package myproject.automation.homework1.tests;

import myproject.automation.homework1.BaseScript;
import myproject.automation.homework1.pages.Nav_Header;
import myproject.automation.homework1.pages.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginTests extends BaseScript {
    public static void main(String[] args) throws InterruptedException {
        String email = "webinar.test@gmail.com";
        String password = "Xcg7299bnSmMuRLp9ITw";

        WebDriver driver = getDriver();
        WebDriverWait wait = createWaitDriver(driver);

        LoginPage loginPage = new LoginPage(driver);
        loginPage.goToLoginPage();
        loginPage.fillEmailInput(email);
        loginPage.fillPasswordInput(password);
        loginPage.clickLoginBtn();

        Nav_Header navHeader = new Nav_Header(driver, wait);
        navHeader.clickOnLogo();
        navHeader.clickOnExitBtn();

        quitDriver(driver);
    }
}
